#![crate_name = "datomaiz"]
#![crate_type = "proc-macro"]

use {
  datom::Datom,
  std::{
    fs::read_to_string, path::Path
  },
  serde::{ Serialize, Deserialize },
  proc_macro::TokenStream,
  proc_quote::quote,
  proc_macro2::TokenStream as TokenStream2,
};

#[cfg(feature = "aski")]
extern crate ron;
#[cfg(feature = "aski")]
use ron::de::from_str;

#[proc_macro]
pub fn datomaizAski(input: TokenStream) -> TokenStream {
  let askiIuniksPath: &Path = Path::new(&input.to_string());
  let askiString = read_to_string(askiIuniksPath)?;
  let datom: Datom = from_str(askiString)?;

  
  TokenStream::from(generateRustFromDatom(datom))
}

fn generateRustFromDatom(datom: Datom) -> TokenStream2 {
  quote! {

  }
}
